package com.rintabt.skillacademy

import android.os.Bundle
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val density = this.resources.displayMetrics.density
        val displayMetrics = this.resources.displayMetrics.widthPixels / density

        val itemAdapter = findViewById<RecyclerView>(R.id.rvDetail)
        itemAdapter.layoutManager = GridLayoutManager(this, displayMetrics.toInt()/70)
        itemAdapter.adapter = DetailAdapter()
    }
}
