package com.rintabt.skillacademy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class MainFragment : Fragment() {

    //onCreateView dipanggil ketika activity sudah dijalankan dan fragment sudah di-inflate
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_main, container, false)
    }

    //onViewCreated digunakan ketika fragment sudah dipanggil dan kita ingin menginisialisasi salah satu di dalam framgent variabel yang ada
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    //onDestroyView dipanggil ketika activity (yang telah dibuka) ditutup
    override fun onDestroyView() {
        super.onDestroyView()
    }
}
