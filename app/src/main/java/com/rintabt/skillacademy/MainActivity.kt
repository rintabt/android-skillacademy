package com.rintabt.skillacademy

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val text = findViewById<TextView>(R.id.tvTextHalo)
        text.setOnClickListener { Toast.makeText(this, "Halo", Toast.LENGTH_SHORT).show() }

        val frameLayout = findViewById<FrameLayout>(R.id.flMain)
        supportFragmentManager.beginTransaction().replace(R.id.flMain, MainFragment()).commit()

        btnMoveToDetail.setOnClickListener {
            val intent = Intent(this, DetailActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
